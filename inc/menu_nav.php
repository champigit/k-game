<div id="menu">
    <ul class="ul_menu">

        <?php
        $query_ray0 = "SELECT id FROM k_post";
        $query_ray0 .= " WHERE type = 'nav'";
        $query_ray0 .= " AND slug = 'menu_nav';";
        $result_ray0 = $Connection->query($query_ray0);
        $nb_ray0 = $result_ray0->num_rows;
        if ($nb_ray0 != 0) {
            $val_ray0 = $result_ray0->fetch_array();

            if ($local != "fr") {
                $LOCAL_PREFIX = $local . "/";
            } else {
                $LOCAL_PREFIX = false;
            }
            $tpl2go = "";
            $query_ray = "SELECT * FROM k_post";
            $query_ray .= " WHERE publish = 'Y'";
            $query_ray .= " AND type = 'nav-item'";
            $query_ray .= " AND id_parent = " . $val_ray0["id"];
            $query_ray .= " ORDER BY ordre;";
            $result_ray = $Connection->query($query_ray);
            $nb_ray = $result_ray->num_rows;
            ?>
            <?php
            while ($val_ray = $result_ray->fetch_array()) {

                $query_sub = "SELECT * FROM k_post WHERE type='nav-item' AND id_parent = " . $val_ray["id"] . " AND publish = 'Y' ORDER BY ordre;";
                $result_sub = $Connection->query($query_sub);
                $nb_sub = $result_sub->num_rows;

                $M_CLASS = "";
                if (isset($val_ray["class"]) && $val_ray["class"] != "") {
                    $M_CLASS = $val_ray["class"];
                }
                if ($val_ray["data"] == "Perso") {
                    $URL2GO = $val_ray["url"];
                } else {
                    $URL2GO = $val_ray["permalink"];
                }
                if ($nb_sub == 0) {
                    echo '<li class="menu_nav ' . $M_CLASS . '"><a href="' . $URL2GO . '">' . $val_ray["title"] . '</a></li>';
                } else {
                    // echo '<li class="menu_nav ' . $M_CLASS . '"><span>' . $val_ray["title"] . '</span>';
                    echo '<li class="menu_nav ' . $M_CLASS . '"><a href="' . $URL2GO . '">' . $val_ray["title"] . '</a>';
                    echo '<ul class="sub_menu">';
                    echo '<li>';
                    echo '<div class="col_l" style="background-image: url(TEMPLATE/' . $M_CLASS . '_1.jpg); background-position: right center; background-repeat: no-repeat; background-size: contain;">';
                    echo '</div>';
                    echo '<div class="col_c">';
                    while ($val_sub = $result_sub->fetch_array()) {
                        echo '<a href="' . $val_sub["permalink"] . '">' . $val_sub["name"] . '</a>';
                    }
                    echo '</div>';
                    echo '<div class="col_r" style="background-image: url(TEMPLATE/' . $M_CLASS . '_2.jpg); background-position: left center; background-repeat: no-repeat; background-size: contain;">';
                    if ($val_ray["url"] != "") {
                        echo '<a href="' . $val_ray["url"] . '">';
                        echo '<img src="TEMPLATE/spacer_menu.png" border ="0" />';
                        echo '</a>';
                    }
                    echo '</div>';
                    echo '</li>';
                    echo '</ul>';
                    echo '</li>';
                }
            }
        }
        ?>
	</ul>
</div>
