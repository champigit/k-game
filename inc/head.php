<!DOCTYPE html>
<html lang="fr">
<head>
    <?php
    include_once ($CORUSCANT."k-custom/inc/functions.php");
    ?>
    <meta charset='UTF-8'/>
    <meta name='viewport' content='width=device-width, initial-scale=1.0'/>
    <title><?php echo $title_add; ?></title>
    <meta itemprop="name" content="<?php echo $title_add; ?>"/>
    <meta name="description" content="<?php echo $DESCRIPTION; ?>"/>
    <meta itemprop="description" content="<?php echo $DESCRIPTION; ?>"/>
    <meta name="Identifier-URL" content="<?php echo $url_site_full; ?>">
    <?php if ($CANONICAL) { ?>
        <link rel="canonical" href="<?php echo $CANONICAL; ?>"/>
    <?php } ?>
    <?php if (!$ONLINE || getConfig("nofollow") == 'Y') { ?>
        <meta name='robots' content='noindex,nofollow'/>
    <?php } ?>
    <base href="<?php echo $url_site_full; ?>">
    <!--[if IE]></base><![endif]-->

    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="k-custom/assets/css/styles.css">
    <link rel="stylesheet" href="k-custom/assets/css/vendors/materialize/materialize.css">

</head>
<body <?php if (isset($ONLOAD)) {
    echo $ONLOAD;
} ?>>