/*
  Created by Pierre Bernardeau.
  For Influa
  Date: 24/12/2019
  Time: 12:24
  
  --- player ---
  
        _
    ,--'_`--.    
  ,/( \   / )\.  
 //  \ \_/ /  \\ 
|/___/     \___\|
((___       ___))    Join the Empire !!!  ﴾̵ ̵◎̵ ̵﴿
|\   \  _  /   /|
 \\  / / \ \  // 
  `\(_/___\_)/'
    `--._.--'
  
 */

jQuery(function ($) {
    $(document).ready(function () {

        $(window).keydown(function (e) {
            var ligs=parseInt($(".grid").data("ligs"));
            var cols=parseInt($(".grid").data("cols"));
            var lig=parseInt($(".player").data("lig"));
            var col=parseInt($(".player").data("col"));
            console.log(lig);
            switch(e.which){
                case 38:
                    if(lig>0){
                        lig=lig-1;
                        console.log('up');
                        $(".player").data("lig",lig);
                        $(".player").css({"grid-row": lig+1});
                    }
                    break;
                case 40:
                    if(lig<ligs-1){
                        lig=lig+1;
                        console.log('down');
                        $(".player").data("lig",lig);
                        $(".player").css({"grid-row": lig+1});
                    }
                    break;
                case 37:
                    if(col>0){
                        col=col-1;
                        console.log('left');
                        $(".player").data("col",col);
                        $(".player").css({"grid-column": col+1});
                    }
                    break;
                case 39:
                    if(col<cols-1){
                        col=col+1;
                        console.log('right');
                        $(".player").data("col",col);
                        $(".player").css({"grid-column": col+1});
                    }
                    break;
            }


            console.log(e.which);
        })



    });
});
