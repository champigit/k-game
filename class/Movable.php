<?php


class Movable
{

    protected $x;
    protected $y;


    public function getLig(){
        return $this->x;
    }
    public function getCol(){
        return $this->y;
    }
    public function setLig($lig){
        $this->x=$lig;
        return $this;
    }
    public function setCol($col){
        $this->y=$col;
        return $this;
    }
    public function __construct($x,$y)
    {
        $this->x=$x;
        $this->y=$y;
    }
}