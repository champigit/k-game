<?php
/*
  Created by Pierre Bernardeau.
  For Influa
  Date: 24/12/2019
  Time: 10:48
  
  --- autoload.php ---
  
        _
    ,--'_`--.    
  ,/( \   / )\.  
 //  \ \_/ /  \\ 
|/___/     \___\|
((___       ___))    Join the Empire !!!  ﴾̵ ̵◎̵ ̵﴿
|\   \  _  /   /|
 \\  / / \ \  // 
  `\(_/___\_)/'
    `--._.--'
  
 */


/**
 * Charge automatiquement les classes du projet
 * @param $class
 */
function autoloader($class){
    $parts = explode('\\' , $class);
    include end($parts).'.php';
}
spl_autoload_register('autoloader');
