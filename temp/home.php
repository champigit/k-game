<!DOCTYPE html>
<html>
	<head>
		<meta name="viewport" content="user-scalable=yes, width=800" />
		<title>SITE EN CONSTRUCTION</title>
			<meta name="description" content="<?= getConfigSite("enseigne"); ?>">
			<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    		<meta http-equiv="Content-Language" content="fr">
    		<meta name="robots" content="noindex,nofollow">
			<link rel="shortcut icon" href="favicon.ico">
            <link href="https://fonts.googleapis.com/css?family=Ubuntu&display=swap" rel="stylesheet">
        <style type="text/css">
	body, td { background: url("k-custom/temp/construction.jpg") center center no-repeat; background-size: cover; font-family: Arial; font-size: 9px; color: #dbb66a; letter-spacing: 2px; }
	#conteneur { margin-left: auto; margin-bottom: 0; margin-right: auto; margin-top: 0; background-position: 0; width: 100%; height: 100%; text-align: left; z-index: 2;}
	#centpourcent { height: 100%;}
	#content { margin-right: 0px; margin-left: 0px; margin-bottom: auto; margin-top: auto; position: relative; height: 600px; width: 800px; }
	</style>
	</head>
		<body>
			<div id="conteneur">
				<table border="0" id="centpourcent" cellpadding="0" cellspacing="0" width="100%">
					<tr>
						<td align="center">
							<div id="content">

							</div>
						</td>
					</tr>
				</table>
	    	</div>
		</body>
	</html>