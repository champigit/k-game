<!--Template: Homepage -->
<?php
// HEAD
include ("k-custom/inc/head.php");
// HEADER
include ("k-custom/inc/header.php");
// CONTENU PERSONNALISÉ
?>

<div id="home">
    <?php
    /**
     * Autoload
     *
     * On charge tous les éléments
     * nécessaires au bon fonctionnement
     * du jeu
     */
    require $CORUSCANT.'/k-custom/class/autoload.php';
    ?>


        <?php
        $grid=array(array());

        $ligs=6;
        $cols=6;

        /**
         * On définit d'abord la grille
         */
        for($lig=0;$lig<$ligs;$lig++){
            for($col=0;$col<$cols;$col++){
                $grid[$lig][$col]=new Kase($lig,$col);
            }
        }

        /**
         * Puis ensuite on dessine la grille
         */
        echo "<div id='home_grid' class='grid' data-ligs='$ligs' data-cols='$cols'>";
        // D'abord un peu de css pour la largeur
        echo "<style>.grid{grid-template-columns: repeat($cols, 30px);}</style>";


        foreach ($grid as $lig){
            foreach ($lig as $case){
                echo "<div class='case' data-lig='".$case->getLig()."' data-col='".$case->getCol()."' style='grid-row:". (intval($case->getLig())+1) ." ;grid-column:". (intval($case->getCol())+1) ." '></div>";
            }
        }


        /**
         * Plaçons notre joueur
         */

        // On génère deux variables aléatoires. Plus tard on vérifiera qu'il n'y a pas d'obstacle pour le joueur à cet emplacement ou bien on indiquera les coordonnées (par ex pour un puzzle)
        $px=random_int(0,$ligs-1);
        $py=random_int(0,$cols-1);
        $player=new Player($px, $py);

        echo "<div class='player' data-lig='".$player->getLig()."' data-col='".$player->getCol()."' style='grid-row:". ($px+1) .";grid-column:". ($py+1) ."'></div>";


    /**
     * On ferme la grille
     */

    echo "</div>";

        ?>





</div>



<?php
// FIN CONTENU PERSONNALISÉ
// FOOTER
include ("k-custom/inc/footer.php");
?>